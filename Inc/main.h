/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
/* Exported types ------------------------------------------------------------*/

/* USER CODE END Includes */

/* Exported macro ------------------------------------------------------------*/

#define KEY_ROW_02_Pin GPIO_PIN_13
#define KEY_ROW_02_GPIO_Port GPIOC
#define KEY_ROW_05_Pin GPIO_PIN_0
#define KEY_ROW_05_GPIO_Port GPIOC
#define KEY_ROW_04_Pin GPIO_PIN_1
#define KEY_ROW_04_GPIO_Port GPIOC
#define KEY_ROW_03_Pin GPIO_PIN_2
#define KEY_ROW_03_GPIO_Port GPIOC
#define SPI2_MOSI_LED_Pin GPIO_PIN_3
#define SPI2_MOSI_LED_GPIO_Port GPIOC
#define KEY_ROW_01_Pin GPIO_PIN_0
#define KEY_ROW_01_GPIO_Port GPIOA
#define KEY_ROW_00_Pin GPIO_PIN_1
#define KEY_ROW_00_GPIO_Port GPIOA
#define EEPROM_nCS_Pin GPIO_PIN_2
#define EEPROM_nCS_GPIO_Port GPIOA
#define EEPROM_nHOLD_Pin GPIO_PIN_3
#define EEPROM_nHOLD_GPIO_Port GPIOA
#define EEPROM_nWP_Pin GPIO_PIN_4
#define EEPROM_nWP_GPIO_Port GPIOA
#define SPI1_SCK_KEY_Pin GPIO_PIN_5
#define SPI1_SCK_KEY_GPIO_Port GPIOA
#define SPI1_SS_GPIO_KEY_Pin GPIO_PIN_6
#define SPI1_SS_GPIO_KEY_GPIO_Port GPIOA
#define SPI1_MOSI_KEY_Pin GPIO_PIN_7
#define SPI1_MOSI_KEY_GPIO_Port GPIOA
#define LED_ROW_DRIVE_05_Pin GPIO_PIN_4
#define LED_ROW_DRIVE_05_GPIO_Port GPIOC
#define LED_ROW_DRIVE_04_Pin GPIO_PIN_5
#define LED_ROW_DRIVE_04_GPIO_Port GPIOC
#define SPI2_SS_GPIO_LED_Pin GPIO_PIN_1
#define SPI2_SS_GPIO_LED_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define SPI2_SCK_LED_Pin GPIO_PIN_10
#define SPI2_SCK_LED_GPIO_Port GPIOB
#define GPIO_CAPS_LOCK_LED3_Pin GPIO_PIN_12
#define GPIO_CAPS_LOCK_LED3_GPIO_Port GPIOB
#define GPIO_NUM_LOCK_LED2_Pin GPIO_PIN_13
#define GPIO_NUM_LOCK_LED2_GPIO_Port GPIOB
#define GPIO_SCROLL_LOCK_LED1_Pin GPIO_PIN_14
#define GPIO_SCROLL_LOCK_LED1_GPIO_Port GPIOB
#define LED_ROW_DRIVE_02_Pin GPIO_PIN_7
#define LED_ROW_DRIVE_02_GPIO_Port GPIOC
#define LED_ROW_DRIVE_01_Pin GPIO_PIN_8
#define LED_ROW_DRIVE_01_GPIO_Port GPIOC
#define LED_ROW_DRIVE_00_Pin GPIO_PIN_9
#define LED_ROW_DRIVE_00_GPIO_Port GPIOC
#define KEY_ROW_06_Pin GPIO_PIN_15
#define KEY_ROW_06_GPIO_Port GPIOA
#define SPI3_SCK_EEPROM_Pin GPIO_PIN_10
#define SPI3_SCK_EEPROM_GPIO_Port GPIOC
#define SPI3_MISO_EEPROM_Pin GPIO_PIN_11
#define SPI3_MISO_EEPROM_GPIO_Port GPIOC
#define SPI3_MOSI_EEPROM_Pin GPIO_PIN_12
#define SPI3_MOSI_EEPROM_GPIO_Port GPIOC
#define LED_ROW_DRIVE_03_Pin GPIO_PIN_4
#define LED_ROW_DRIVE_03_GPIO_Port GPIOB
#define I2C1_SCL_OLED_Pin GPIO_PIN_6
#define I2C1_SCL_OLED_GPIO_Port GPIOB
#define I2C1_SDA_OLED_Pin GPIO_PIN_7
#define I2C1_SDA_OLED_GPIO_Port GPIOB
#define TIM4_CH3_LED4_Pin GPIO_PIN_8
#define TIM4_CH3_LED4_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the
  *        HAL drivers code
  */
#define USE_FULL_ASSERT    1U

/* USER CODE BEGIN Private defines */
// keyboard related private defines

#define KEYBOARD_ROW_NUMBER (6)      // no more than 32
#define KEYBOARD_COLUMN_NUMBER (16)  // no more than 32
#define KEYBOARD_CONFIGURATION_COUNT (9)

#define KEYBOARD_LAYER_NUMBER (2)
#define KEYBOARD_MAX_FN_KEY_NUMBER (4)
#define HID_BUFFER_SIZE (8)

#define UART1_RX_BUF_SIZE (64)
#define UART1_TX_BUF_SIZE (64)

// *****************************************************************************
// key layout defination
// last line: Fn, Ctrl, Super, Alt, blank, Space, Space, Space, Space, Insert, Alt, Ctrl, Fn, Left, Down, Right
//            0     1     2     3     4      5       6     7      8     10    Super  11   12    13   14    15


// ************************************
// https://gist.github.com/MightyPork/6da26e382a7ad91b5496ee55fdc73db2
// or refer to USB HIS usage table

#define NUMBER_OF_KEYS      (91)

#define K_UNDEFINED         (0)
#define K_F1                (0x3A)
#define K_F2                (0x3B)
#define K_F3                (0x3C)
#define K_F4                (0x3D)
#define K_F5                (0x3E)
#define K_F6                (0x3F)
#define K_F7                (0x40)
#define K_F8                (0x41)
#define K_F9                (0x42)
#define K_F10               (0x43)
#define K_F11               (0x44)
#define K_F12               (0x45)

#define K_ESC               (0x29)
#define K_BACK_QUOTE        (0x35) // modified according to cherry G83 keyboard

#define K_MINUS             (0x2D)
#define K_EQUAL             (0x2E)

#define K_DEL               (0x4C)

#define K_SCROLL_LOCK       (0x47)
#define K_TAB               (0x2B)

#define K_BRACKET_LEFT      (0x2F)
#define K_BRACKET_RIGHT     (0X30)  // name longest 15 characters
#define K_BACK_SLASH        (0x31)
#define K_BACKSPACE         (0x2A)

#define K_PAUSE_BREAK       (0x48)
// #define K_MUTE              (0x7F) // in hid usage pdf ERROR
// #define K_VOL_UP            (0x80) // in hid usage pdf ERROR
// #define K_VOL_DOWN          (0x81) // in hid usage pdf ERROR

#define K_MUTE              (0xE2)
// capture from USB keyboard whole report is 3 bytes 0x02 0xE2 0x00
#define K_VOL_UP            (0xE9) // capture from USB keyboard
#define K_VOL_DOWN          (0xEA) // capture from USB keyboard
#define K_PLAY_PREVIOUS     (0xB6)
#define K_PLAY_PLAY         (0xCD)
#define K_PLAY_NEXT         (0xB5)
#define K_PLAY_STOP         (0xB7)


#define K_A                 (0x04)
#define K_B                 (0x05)
#define K_C                 (0x06)
#define K_D                 (0x07)
#define K_E                 (0x08)
#define K_F                 (0x09)
#define K_G                 (0x0A)
#define K_H                 (0x0B)
#define K_I                 (0x0C)
#define K_J                 (0x0D)
#define K_K                 (0x0E)
#define K_L                 (0x0F)
#define K_M                 (0x10)
#define K_N                 (0x11)
#define K_O                 (0x12)
#define K_P                 (0x13)
#define K_Q                 (0x14)
#define K_R                 (0x15)
#define K_S                 (0x16)
#define K_T                 (0x17)
#define K_U                 (0x18)
#define K_V                 (0x19)
#define K_W                 (0x1A)
#define K_X                 (0x1B)
#define K_Y                 (0x1C)
#define K_Z                 (0x1D)

#define K_1                 (0x1E)
#define K_2                 (0x1F)
#define K_3                 (0x20)
#define K_4                 (0x21)
#define K_5                 (0x22)
#define K_6                 (0x23)
#define K_7                 (0x24)
#define K_8                 (0x25)
#define K_9                 (0x26)
#define K_0                 (0x27)


#define K_SEMICOLON         (0x33)
#define K_QUOTE             (0x34)
#define K_ENTER             (0x28)
#define K_HOME              (0x4A)
#define K_END               (0x4D)

#define K_PRINTSCREEN       (0x46)


#define K_COMMA             (0x36)
#define K_PERIOD            (0x37)
#define K_SLASH             (0x38)
#define K_PAGE_UP           (0x4B)
#define K_UP                (0x52)
#define K_DOWN              (0x51)
#define K_LEFT              (0x50)
#define K_RIGHT             (0x4F)
#define K_PAGE_DOWN         (0x4E)
#define K_MENU              (0x76)  // not used
#define K_APPLICATION       (0x65)

#define K_CAPS_LOCK         (0x39)
#define K_NUM_LOCK          (0x53) // Keyboard Num Lock and Clear


#define K_SPACE             (0x2C)
#define K_INSERT            (0x49)


// all FN keys are defined as 0x201 + index, the first is 0x201
#define K_FN_LEFT           (0x201)
#define K_FN_RIGHT          (0x202)

#define K_CTRL_LEFT         (0x1E0)
#define K_SHIFT_LEFT        (0x1E1)
#define K_ALT_LEFT          (0x1E2)
#define K_SUPER_LEFT        (0x1E3)
#define K_CTRL_RIGHT        (0X1E4)
#define K_SHIFT_RIGHT       (0X1E5)
#define K_ALT_RIGHT         (0x1E6)
#define K_SUPER_RIGHT       (0x1E7)

// ************************************


/* USER CODE END Private defines */

#ifdef __cplusplus
extern "C" {
#endif
void _Error_Handler (char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */

/**
  * @}
*/

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
