** The Ultimate Hacking Matrix **  
Author: "Rafael Lee" email:rafaellee.img@gmail.com  
作者: Rafael Lee 邮箱：rafaellee.img@gmail.com  

This project is under GPL v3. For detail, please refers to the file COPYING.  
这个工程的许可是 GPL v3. 详细的许可在 COPYING 文件中。  

Finally I named the keyboard Ultimate Hacking Matrix.  
我终于给键盘起了个名字：The Ultimate Hacking Matrix.  


You are not buying only a device, you can ask me to modify the key layout to what you want and program it into Ultimate Hacking Matrix. But there is no guarantee I can wrote proper program for very hard or complicated cases.  
用户购买到的不止是一个设备，我可以根据需求为用户编写 Ultimate Hacking Matrix 的 layout 的程序。但是，如果要求很难或者很复杂我可能写不出。  


This Ultimate Hacking Matrix will use Cherry keys in default, I had a set of black keys, a set of red keys and a set of blue keys.  
标配 Cherry 轴，我准备了黑轴，红轴，青轴各一套。  


Until now the Ultimate Hacking Matrix project has been started for more than 2 years. I spent more than 1 year reading standards and protocols of USB. One year before, I made version 1. I use it every day. The software I used it for 1 year well without any modification.  
Ultimate Hacking Matrix 项目已经开始两年有余，我花了一年以上研究 USB 协议，一年前，有了第一版。我每天都用它，键盘软件工作良好，我用了一年无需更改。  

If you find any minor problem, email me. I will try to figure it out.  
如果你发现什么问题，给我发邮件，我会想办法。  


Known Problems: Scroll Lock LED doesn't work。  
已知的问题： Scroll Lock 灯不工作。  


Features I have in mind but not in the firmware yet:  
我计划实现单没实现的功能：  
1, Add multi media keys support. For example: Vol +, Vol -  
1, 多媒体按键支持，例如 Vol +、 Vol - 。  


If you want any new features, please have a discuss with me.  
如果你希望新功能，请和我商量一下。  


Perpendicular layout is designed for who put keyboard direct in front of you instead of normal keyboard put in front left of you.  
垂直的键位设计是为了把它放在最正前方，而不是像一般键盘放在左前方。  


The Ultimate Hacking Matrix is the last keyboard you would like to have.  
Ultimate Hacking Matrix 将是你的最后一把键盘。  


You can make your own keyboard using part or all of this project under the license of GPL v3.  
你可以根据此项目造自己的键盘，必须遵守 GPL v3 协议。  


You will have my word of improving the quality and functions of the software but there is absolute no warranty. I will test the Ultimate Hacking Matrix before deliver. If the Ultimate Hacking Matrix malfunctions, try to plug it again, then figure it out in common sense. If anything remains, please sent me email.  
我保证会持续更新软件，加入新功能，但是 Ultimate Hacking Matrix 本身没有质保，出售前我会测试。如果键盘有任何问题，首先请重新拔插，试着用常识分析。如果不能解决，请给我发邮件。  


No refund is available.  
不设退款。  


If it is broken I will fix it. Reasonable fee may exist for fixing.  
如果损坏，我会修理。可能会适当收费。  

If you want to buy, please contact me.  
如果想购买，请联系我。  

The price is 2000 CNY or 300 USD, if you do not want to prepare the keys and key caps, the price is 1700, if you want to have a component kit, the price is 1500.  
Ultimate Hacking Matrix 的价格是 2000 CNY 或 300 USD ，如果不需要键帽和轴，价格是 1700，套件价格 1500.  



