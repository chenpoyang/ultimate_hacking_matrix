/**
  ******************************************************************************
  * @file    usbd_hid.c
  * @author  MCD Application Team
  * @version V2.4.2
  * @date    11-December-2015
  * @brief   This file provides the HID core functions.
  *
  * @verbatim
  *
  *          ===================================================================
  *                                HID Class  Description
  *          ===================================================================
  *           This module manages the HID class V1.11 following the "Device Class Definition
  *           for Human Interface Devices (HID) Version 1.11 Jun 27, 2001".
  *           This driver implements the following aspects of the specification:
  *             - The Boot Interface Subclass
  *             - The Mouse protocol
  *             - The Keyboard protocol
  *             - Usage Page : Generic Desktop
  *             - Usage : Joystick
  *             - Collection : Application
  *
  * @note     In HS mode and when the DMA is used, all variables and data structures
  *           dealing with the DMA during the transaction process should be 32-bit aligned.
  *
  *
  *  @endverbatim
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usbd_hid.h"
#include "usbd_desc.h"
#include "usbd_ctlreq.h"


/** @addtogroup STM32_USB_DEVICE_LIBRARY
  * @{
  */


/** @defgroup USBD_HID
  * @brief usbd core module
  * @{
  */

/** @defgroup USBD_HID_Private_TypesDefinitions
  * @{
  */
/**
  * @}
  */


/** @defgroup USBD_HID_Private_Defines
  * @{
  */

/**
  * @}
  */


/** @defgroup USBD_HID_Private_Macros
  * @{
  */
/**
  * @}
  */




/** @defgroup USBD_HID_Private_FunctionPrototypes
  * @{
  */


static uint8_t  USBD_HID_Init (USBD_HandleTypeDef *pdev,
                               uint8_t cfgidx);

static uint8_t  USBD_HID_DeInit (USBD_HandleTypeDef *pdev,
                                 uint8_t cfgidx);

static uint8_t  USBD_HID_Setup (USBD_HandleTypeDef *pdev,
                                USBD_SetupReqTypedef *req);

static uint8_t  *USBD_HID_GetCfgDesc (uint16_t *length);

static uint8_t  *USBD_HID_GetDeviceQualifierDesc (uint16_t *length);

static uint8_t  USBD_HID_DataIn (USBD_HandleTypeDef *pdev, uint8_t epnum);
static uint8_t  USBD_HID_DataOut (USBD_HandleTypeDef *pdev, uint8_t epnum);

extern uint8_t set_lock_keys_indicator (uint8_t USBState);
/**
  * @}
  */

/** @defgroup USBD_HID_Private_Variables
  * @{
  */

USBD_ClassTypeDef  USBD_HID =
{
  USBD_HID_Init,
  USBD_HID_DeInit,
  USBD_HID_Setup,
  NULL, /*EP0_TxSent*/
  NULL, /*EP0_RxReady*/
  USBD_HID_DataIn, /*DataIn*/
  USBD_HID_DataOut, /*DataOut*/
  NULL, /*SOF */
  NULL,
  NULL,
  USBD_HID_GetCfgDesc,
  USBD_HID_GetCfgDesc,
  USBD_HID_GetCfgDesc,
  USBD_HID_GetDeviceQualifierDesc,
};

/* USB HID device Configuration Descriptor */
__ALIGN_BEGIN static uint8_t USBD_HID_CfgDesc[USB_HID_CONFIG_DESC_SIZ]  __ALIGN_END =
{
  0x09, /* bLength: Configuration Descriptor size */
  USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType: Configuration */
  USB_HID_CONFIG_DESC_SIZ,
  /* wTotalLength: Bytes returned */
  0x00,
  0x01,         /*bNumInterfaces: 1 interface*/
  0x01,         /*bConfigurationValue: Configuration value*/
  0x00,         /*iConfiguration: Index of string descriptor describing
  the configuration*/
  0xE0,         /*bmAttributes: bus powered and Support Remote Wake-up */
  0x32,         /*MaxPower 100 mA: this current is used for detecting Vbus*/

  /************** Descriptor of Joystick Mouse interface ****************/
  /* 09 */
  0x09,         /*bLength: Interface Descriptor size*/
  USB_DESC_TYPE_INTERFACE,/*bDescriptorType: Interface descriptor type*/
  0x00,         /*bInterfaceNumber: Number of Interface*/
  0x00,         /*bAlternateSetting: Alternate setting*/
  0x02,         /*bNumEndpoints*/
  0x03,         /*bInterfaceClass: HID*/
  0x01,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
  0x01,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
  0,            /*iInterface: Index of string descriptor*/
  /******************** Descriptor of Joystick Mouse HID ********************/
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  HID_KEYBOARD_REPORT_DESC_SIZE,/*wItemLength: Total length of Report descriptor*/
  0x00,
  /******************** Descriptor of Keyboard endpoint ********************/
  /* 27 */
  0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/

  HID_EPIN_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x03,          /*bmAttributes: Interrupt endpoint*/
  HID_EPIN_SIZE, /*wMaxPacketSize: 8 Byte max */
  0x00,
  HID_FS_BINTERVAL,          /*bInterval: Polling Interval (10 ms)*/

  /******************** Descriptor of LEDs endpoint ********************/
  /* 34 */
  0x07,                 /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT,     /*bDescriptorType:*/
  HID_EPOUT_ADDR,           /*bEndpointAddress: Endpoint Address (OUT)*/
  0x03,                 /*bmAttributes: Interrupt endpoint*/
  HID_EPOUT_SIZE,         /*wMaxPacketSize: 1 Byte max */
  0x00,
  HID_FS_BINTERVAL,                 /*bInterval: Polling Interval (10 ms)*/
  /* 41 */
} ;

/* USB HID device Configuration Descriptor */
__ALIGN_BEGIN static uint8_t USBD_HID_Desc[USB_HID_DESC_SIZ]  __ALIGN_END  =
{
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  HID_KEYBOARD_REPORT_DESC_SIZE, /*wItemLength: Total length of Report descriptor*/
  0x00,
};

/* USB Standard Device Descriptor */
__ALIGN_BEGIN static uint8_t USBD_HID_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC]  __ALIGN_END =
{
  USB_LEN_DEV_QUALIFIER_DESC,
  USB_DESC_TYPE_DEVICE_QUALIFIER,
  0x00,
  0x02,
  0x00,
  0x00,
  0x00,
  0x40,
  0x01,
  0x00,
};

__ALIGN_BEGIN static uint8_t HID_MOUSE_ReportDesc[HID_MOUSE_REPORT_DESC_SIZE]  __ALIGN_END =
{
  0x05,   0x01,
  0x09,   0x02,
  0xA1,   0x01,
  0x09,   0x01,

  0xA1,   0x00,
  0x05,   0x09,
  0x19,   0x01,
  0x29,   0x03,

  0x15,   0x00,
  0x25,   0x01,
  0x95,   0x03,
  0x75,   0x01,

  0x81,   0x02,
  0x95,   0x01,
  0x75,   0x05,
  0x81,   0x01,

  0x05,   0x01,
  0x09,   0x30,
  0x09,   0x31,
  0x09,   0x38,

  0x15,   0x81,
  0x25,   0x7F,
  0x75,   0x08,
  0x95,   0x03,

  0x81,   0x06,
  0xC0,   0x09,
  0x3c,   0x05,
  0xff,   0x09,

  0x01,   0x15,
  0x00,   0x25,
  0x01,   0x75,
  0x01,   0x95,

  0x02,   0xb1,
  0x22,   0x75,
  0x06,   0x95,
  0x01,   0xb1,

  0x01,   0xc0
};

/**
  * @}
  */


/* HID的报告描述符*/
/*定义了8字节发送：
**  第一字节表示特殊件是否按下：D0:Ctrl D1:Shift D2:Alt
**  第二字节保留，值为0
**  第三~第八字节:普通键键值的数组,最多能同时按下6个键
**定义了1字节接收:对应键盘上的LED灯,这里只用了两个位。
**  D0:Num Lock   D1:Cap Lock   D2:Scroll Lock   D3:Compose   D4:Kana*/
__ALIGN_BEGIN static uint8_t HID_KEYBOARD_ReportDesc[HID_KEYBOARD_REPORT_DESC_SIZE]  __ALIGN_END =
{
  /*short Item   D7~D4:bTag;D3~D2:bType;D1~D0:bSize
      **bTag ---主条目   1000:输入(Input) 1001:输出(Output) 1011:特性(Feature) 1010:集合(Collection) 1100:关集合(End Collection)
      **      全局条目  0000:用途页(Usage Page) 0001:逻辑最小值(Logical Minimum) 0010:逻辑最大值(Logical Maximum) 0011:物理最小值(Physical Minimum)
      **          0100:物理最大值(Physical Maximum) 0101:单元指数(Unit Exponet) 0110:单元(Unit) 0111:数据域大小(Report Size)
      **          1000:报告ID(Report ID) 1001:数据域数量(Report Count) 1010:压栈(Push) 1011:出栈(Pop) 1100~1111:保留(Reserved)
      **      局部条目  0000:用途(Usage) 0001:用途最小值(Usage Minimum) 0010:用途最大值(Usage Maximum) 0011:标识符索引(Designator Index)
      **          0100:标识符最小值(Designator Minimum) 0101:标识符最大值(Designator Maximum) 0111:字符串索引(String Index) 1000:字符串最小值(String Minimum)
      **          1001:字符串最大值(String Maximum) 1010:分隔符(Delimiter) 其他：保留(Reserved)
      **bType---00:主条目(main)  01:全局条目(globle)  10:局部条目(local)  11:保留(reserved)
      **bSize---00:0字节  01:1字节  10:2字节  11:4字节*/

  //0x05:0000 01 01 这是个全局条目，用途页选择为普通桌面页
  0x05, 0x01, // USAGE_PAGE (Generic Desktop)
  //0x09:0000 10 01 这是个全局条目，用途选择为键盘
  0x09, 0x06, // USAGE (Keyboard)
  //0xa1:1010 00 01 这是个主条目，选择为应用集合，
  0xa1, 0x01, // COLLECTION (Application)



  //0x05:0000 01 11 这是个全局条目，用途页选择为键盘/按键
  0x05, 0x07, // USAGE_PAGE (Keyboard/Keypad)

  //0x19:0001 10 01 这是个局部条目，用途的最小值为0xe0，对应键盘上的左ctrl键
  0x19, 0xe0, // USAGE_MINIMUM (Keyboard LeftControl)
  //0x29:0010 10 01 这是个局部条目，用途的最大值为0xe7，对应键盘上的有GUI(WIN)键
  0x29, 0xe7, // USAGE_MAXIMUM (Keyboard Right GUI)
  //0x15:0001 01 01 这是个全局条目，说明数据的逻辑值最小值为0
  0x15, 0x00, // LOGICAL_MINIMUM (0)
  //0x25:0010 01 01 这是个全局条目，说明数据的逻辑值最大值为1
  0x25, 0x01, // LOGICAL_MAXIMUM (1)

  //0x95:1001 01 01 这是个全局条目，数据域的数量为8个
  0x95, 0x08, // REPORT_COUNT (8)
  //0x75:0111 01 01 这是个全局条目，每个数据域的长度为1位
  0x75, 0x01, // REPORT_SIZE (1)
  //0x81:1000 00 01 这是个主条目，有8*1bit数据域作为输入，属性为:Data,Var,Abs
  0x81, 0x02, // INPUT (Data,Var,Abs)

  //0x95:1001 01 01 这是个全局条目,数据域的数量为1个
  0x95, 0x01, // REPORT_COUNT (1)
  //0x75:0111 01 01 这是个全局条目，每个数据域的长度为8位
  0x75, 0x08, // REPORT_SIZE (8)
  //0x81:1000 00 01 这是个主条目，有1*8bit数据域作为输入，属性为:Cnst,Var,Abs
  0x81, 0x03, // INPUT (Cnst,Var,Abs)

  //0x95:1001 01 01 这是个全局条目，数据域的数量为6个
  0x95, 0x06, // REPORT_COUNT (6)
  //0x75:0111 01 01 这是个全局条目，每个数据域的长度为8位
  0x75, 0x08, // REPORT_SIZE (8)
  //0x25:0010 01 01 这是个全局条目，逻辑最大值为255
  0x25, 0xFF, // LOGICAL_MAXIMUM (255)
  //0x19:0001 10 01 这是个局部条目，用途的最小值为0
  0x19, 0x00, // USAGE_MINIMUM (Reserved (no event indicated))
  //0x29:0010 10 01 这是个局部条目，用途的最大值为0x65
  0x29, 0x65, // USAGE_MAXIMUM (Keyboard Application)
  //0x81:1000 00 01 这是个主条目，有6*8bit的数据域作为输入，属相为属性为:Data,Var,Abs
  0x81, 0x00, // INPUT (Data,Ary,Abs)

  //0x25:0010 01 01 这是个全局条目，逻辑的最大值为1
  0x25, 0x01, // LOGICAL_MAXIMUM (1)
  //0x95:1001 01 01 这是个全局条目，数据域的数量为2
  0x95, 0x02, // REPORT_COUNT (2)
  //0x75:0111 01 01 这是个全局条目，每个数据域的长度为1位
  0x75, 0x01, // REPORT_SIZE (1)
  //0x05:0000 01 01 这是个全局条目，用途页选择为LED页
  0x05, 0x08, // USAGE_PAGE (LEDs)
  //0x19:0001 10 01 这是个局部条目，用途的最小值为0x01,对应键盘上的Num Lock
  0x19, 0x01, // USAGE_MINIMUM (Num Lock)
  //0x29:0010 10 01 这是个局部条目，用途的最大值为0x02,对应键盘上的Caps Lock
  0x29, 0x02, // USAGE_MAXIMUM (Caps Lock)
  //0x91:1001 00 01 这是个主条目，有2*1bit的数据域作为输出，属性为:Data,Var,Abs
  0x91, 0x02, // OUTPUT (Data,Variable,Absolute) // LED report

  //0x95:1001 01 01 这是个全局条目，数据域的数量为1个
  0x95, 0x01, // REPORT_COUNT (1)
  //0x75:0111 01 01 这是个全局条目，每个数据域的长度为6bit,正好与前面的2bit组成1字节
  0x75, 0x06, // REPORT_SIZE (6)
  //0x91:1001 00 01 这是个主条目，有1*6bit数据域最为输出，属性为:Cnst,Var,Abs
  0x91, 0x03, // OUTPUT (Cnst,Var,Abs)

  0xc0        // END_COLLECTION
};
/** @defgroup USBD_HID_Private_Functions
  * @{
  */

static uint8_t rx_buf;

/**
  * @brief  USBD_HID_Init
  *         Initialize the HID interface
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_HID_Init (USBD_HandleTypeDef *pdev,
                               uint8_t cfgidx)
{
  uint8_t ret = 0;

  /* Open EP IN */
  USBD_LL_OpenEP (pdev,
                  HID_EPIN_ADDR,
                  USBD_EP_TYPE_INTR,
                  HID_EPIN_SIZE);

  /* Open EP OUT */
  USBD_LL_OpenEP (pdev,
                  HID_EPOUT_ADDR,
                  USBD_EP_TYPE_INTR,
                  HID_EPOUT_SIZE);

  //set EP_OUT 1 prepared to received the data
  USBD_LL_PrepareReceive (pdev, HID_EPOUT_ADDR, &rx_buf, 1);

  pdev->pClassData = USBD_malloc (sizeof (USBD_HID_HandleTypeDef));

  if (pdev->pClassData == NULL)
  {
    ret = 1;
  }
  else
  {
    ((USBD_HID_HandleTypeDef *)pdev->pClassData)->state = HID_IDLE;
  }
  return ret;
}

/**
  * @brief  USBD_HID_Init
  *         DeInitialize the HID layer
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_HID_DeInit (USBD_HandleTypeDef *pdev,
                                 uint8_t cfgidx)
{
  /* Close HID EPs */
  USBD_LL_CloseEP (pdev,
                   HID_EPIN_ADDR);

  /* FRee allocated memory */
  if (pdev->pClassData != NULL)
  {
    USBD_free (pdev->pClassData);
    pdev->pClassData = NULL;
  }

  return USBD_OK;
}

/**
  * @brief  USBD_HID_Setup
  *         Handle the HID specific requests
  * @param  pdev: instance
  * @param  req: usb requests
  * @retval status
  */
static uint8_t  USBD_HID_Setup (USBD_HandleTypeDef *pdev,
                                USBD_SetupReqTypedef *req)
{
  uint16_t len = 0;
  uint8_t  *pbuf = NULL;
  USBD_HID_HandleTypeDef     *hhid = (USBD_HID_HandleTypeDef *) pdev->pClassData;

  switch (req->bmRequest & USB_REQ_TYPE_MASK)
  {
    case USB_REQ_TYPE_CLASS :
      switch (req->bRequest)
      {


        case HID_REQ_SET_PROTOCOL:
          hhid->Protocol = (uint8_t) (req->wValue);
          break;

        case HID_REQ_GET_PROTOCOL:
          USBD_CtlSendData (pdev,
                            (uint8_t *)&hhid->Protocol,
                            1);
          break;

        case HID_REQ_SET_IDLE:
          hhid->IdleState = (uint8_t) (req->wValue >> 8);
          break;

        case HID_REQ_GET_IDLE:
          USBD_CtlSendData (pdev,
                            (uint8_t *)&hhid->IdleState,
                            1);
          break;

        default:
          USBD_CtlError (pdev, req);
          return USBD_FAIL;
      }
      break;

    case USB_REQ_TYPE_STANDARD:
      switch (req->bRequest)
      {
        case USB_REQ_GET_DESCRIPTOR:
          if (req->wValue >> 8 == HID_REPORT_DESC)
          {
            len = MIN (HID_KEYBOARD_REPORT_DESC_SIZE, req->wLength);
            pbuf = HID_KEYBOARD_ReportDesc;
          }
          else if (req->wValue >> 8 == HID_DESCRIPTOR_TYPE)
          {
            pbuf = USBD_HID_Desc;
            len = MIN (USB_HID_DESC_SIZ, req->wLength);
          }

          USBD_CtlSendData (pdev,
                            pbuf,
                            len);

          break;

        case USB_REQ_GET_INTERFACE :
          USBD_CtlSendData (pdev,
                            (uint8_t *)&hhid->AltSetting,
                            1);
          break;

        case USB_REQ_SET_INTERFACE :
          hhid->AltSetting = (uint8_t) (req->wValue);
          break;
      }
  }
  return USBD_OK;
}

/**
  * @brief  USBD_HID_SendReport
  *         Send HID Report
  * @param  pdev: device instance
  * @param  buff: pointer to report
  * @retval status
  */
uint8_t USBD_HID_SendReport (USBD_HandleTypeDef  *pdev,
                             uint8_t *report,
                             uint16_t len)
{
  USBD_HID_HandleTypeDef     *hhid = (USBD_HID_HandleTypeDef *)pdev->pClassData;

  if (pdev->dev_state == USBD_STATE_CONFIGURED)
  {
    if (hhid->state == HID_IDLE)
    {
      hhid->state = HID_BUSY;
      USBD_LL_Transmit (pdev,
                        HID_EPIN_ADDR,
                        report,
                        len);
    }
  }
  return USBD_OK;
}

/**
  * @brief  USBD_HID_GetPollingInterval
  *         return polling interval from endpoint descriptor
  * @param  pdev: device instance
  * @retval polling interval
  */
uint32_t USBD_HID_GetPollingInterval (USBD_HandleTypeDef *pdev)
{
  uint32_t polling_interval = 0;

  /* HIGH-speed endpoints */
  if (pdev->dev_speed == USBD_SPEED_HIGH)
  {
    /* Sets the data transfer polling interval for high speed transfers.
     Values between 1..16 are allowed. Values correspond to interval
     of 2 ^ (bInterval-1). This option (8 ms, corresponds to HID_HS_BINTERVAL */
    polling_interval = (((1 << (HID_HS_BINTERVAL - 1))) / 8);
  }
  else   /* LOW and FULL-speed endpoints */
  {
    /* Sets the data transfer polling interval for low and full
    speed transfers */
    polling_interval =  HID_FS_BINTERVAL;
  }

  return ((uint32_t) (polling_interval));
}

/**
  * @brief  USBD_HID_GetCfgDesc
  *         return configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_HID_GetCfgDesc (uint16_t *length)
{
  *length = sizeof (USBD_HID_CfgDesc);
  return USBD_HID_CfgDesc;
}


/**
  * @brief  USBD_HID_DataIn
  *         handle data IN Stage
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t  USBD_HID_DataIn (USBD_HandleTypeDef *pdev,
                                 uint8_t epnum)
{

  /* Ensure that the FIFO is empty before a new transfer, this condition could
  be caused by  a new transfer before the end of the previous transfer */
  ((USBD_HID_HandleTypeDef *)pdev->pClassData)->state = HID_IDLE;
  return USBD_OK;
}


static uint8_t USBD_HID_DataOut (USBD_HandleTypeDef *pdev, uint8_t epnum)
{
  HAL_PCD_EP_Receive (pdev->pData, HID_EPOUT_ADDR, &rx_buf, 1);
  set_lock_keys_indicator (rx_buf);
  return USBD_OK;
}



/**
* @brief  DeviceQualifierDescriptor
*         return Device Qualifier descriptor
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
static uint8_t  *USBD_HID_GetDeviceQualifierDesc (uint16_t *length)
{
  *length = sizeof (USBD_HID_DeviceQualifierDesc);
  return USBD_HID_DeviceQualifierDesc;
}

/**
  * @}
  */


/**
  * @}
  */


/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
